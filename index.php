<?php session_start(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
   <title>S5- A1: Login Page</title>
</head>
<body class="container px-5 w-75">

   <div id="alerts" class="my-3">

   <?php if(!isset($_SESSION['loggedIn'])): ?>
      <form action="/server.php" method="post" class="alert alert-primary px-5">
            <h3 class="mb-3">Login</h3>
            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">Email Address: </span>
                <input type="email" class="form-control" name="email" />
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="inputGroup-sizing-default">Password: </span>
                <input type="password" class="form-control" name="password" />
            </div>
            <div class="mb-3 text-end">
                <button type="submit" class="btn btn-primary shadow px-5">Login</button>
            </div>
        </form>
        
   <?php else: ?>

   <form action="/server.php" method="post" class="text-center alert alert-secondary px-5">
      <h4>Hello, johnsmith@gmail.com</h4>
      <button type="submit" class="btn btn-warning shadow px-5">Logout</button>
   </form>
   <?php endif ?>

   </div>
</body>
</html>